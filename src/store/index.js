/* eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'
import VuexORM from '@vuex-orm/core'

Vue.use(Vuex)

import Event from './models/Event/Model'
import eventModule from './models/Event/Module'

const database = new VuexORM.Database()

// Register Model and Module. The First argument is the Model, and
// second is the Module.
database.register(Event, eventModule)

const store = new Vuex.Store({
  plugins: [VuexORM.install(database)],
  state: {},
  actions: {},
  mutations: {},
  getters: {}
})

export default store

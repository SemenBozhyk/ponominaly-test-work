# Ponominalu Senior front-end test work
Demo: https://semenbozhyk-cv.firebaseapp.com/ponominalu/

# Stack:

  - REST API
  - Vue.js, Vuex, VuexORM
  - Firebase hosting
  - SSR (not done yet)

# Installation
```sh
yarn
npm run dev
```
or
```sh
npm i
npm run dev
```
visit http://localhost:8080

# My comments
Nice that backend has structure like REST API. Because of this it's possible to use model-like architecture to communicate with backend.
Mode-like archicture makes great speed up for engineer work. Because of one method which is working with model and backend in one time we can easely dont care about error handling and etc. Also it's allowing make SSR as fast as possible


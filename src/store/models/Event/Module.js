import Vue from 'vue'

export default {
  state: {
    sort: 'avg_weight',
    id: 'id',
    fields: ['seo','categories','tags','subevents.venue','subevents.description']
  },

  mutations: {
    not_found (state, { id, searchingKey }) {
      const errorData = {error: 404, ts: new Date().getTime()}
      errorData[`$${searchingKey}`] = id
      if (!state[id]) {
        Vue.set(state.data, id, errorData)
      }
    },
    set_pagination (state, { page, ids }) {
      if (!state.pagination) {
        Vue.set(state, 'pagination', {})
      }
      Vue.set(state.pagination, page, {page: ids, ts: new Date().getTime()})
    }
  },

  getters: {
    page: (state) => (page) => {
      if (state.pagination) {
        return state.pagination[page]
      } else {
        return false
      }
    },
    id (state) {
      return state.id
    },
    fields: (state) => (fields) => {
      const concat = state.fields.concat(fields)
      const result = concat.reduce((obj, whatever) => {
        obj[whatever] = true
        return obj
      },{})
      return Object.keys(result).join(',')
    },
    get_error: (state) => (id) => {
      return state.data[id] ? state.data[id].error : 'pending'
    }
  }
}

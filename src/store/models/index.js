/* eslint-disable */
import Event from './Event/Model'
import eventModule from './Event/Module'

export default {
  events: {
    Model: Event,
    Module: eventModule
  }
}

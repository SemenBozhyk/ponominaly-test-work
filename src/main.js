import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import adapter from './utils/adapter/index'
import Preloader from './components/Preloader.vue'

import apiConfig from '../config/api'
Vue.config.productionTip = false

Vue.prototype.$api = new Vue({
  ...adapter({
    baseURL: apiConfig.baseURL,
    params: {
      session: '123'
    }
  }),
  store
})

const globalMixins = {
  install: function (self) {
    self.mixin({
      components: {
        Preloader
      }
    })
  }
}

Vue.use(globalMixins)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})

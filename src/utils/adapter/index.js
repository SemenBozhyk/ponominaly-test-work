import axios from 'axios'
import * as methods from './methods'
import * as models from 'store/models'

const serialize = function (model, data) {
  console.log(models)
  const Model = models.default[model].Model ? new models.default[model].Model(data) : false
  return Model && Model.serialize ? Model.serialize : data
}

export default function (options) {
  const ax = axios.create(options)

  // Модуль имеет базовый функционал для выполнения тестовой задачи
  // И никак не есть полноценным решением для общения с бэкендом
  return {
    data: () => ({
      // Время ожидания на повторный запрос данных
      // Откровенно говоря без него все будет падать в луп
      // Но это и не костыль. С помощью этой штуки можно организовать повторные запросы на 5* ошибки
      waitFor: 15000
    }),
    methods: {
      findRecord ({model, id, query = {}}) {
        const ts = new Date().getTime()
        const idToSearch = this.$store.getters[`entities/${model}/id`]
        const fields = this.$store.getters[`entities/${model}/fields`](query.fields)
        const searchId = +id ? +id : id + ''
        const oldData = this.$store.getters[`entities/${model}/find`](searchId)
        // const oldData = this.$store.getters[`entities/${model}/query`]().where(idToSearch, searchId).first()

        delete query.fields
        if (!oldData || oldData.ts + this.waitFor < ts) {
          // if (idType) {
          //   query[idType] = id
          // } else {
          //   query['id'] = id
          // }
          ax.get(`${model}/list`, {
            params: {
              id,
              fields,
              ...query
            }
          }).then(resp => {
            const data = resp.data
            console.log(resp.data)
            // Нет обработки ошибки code: 0 ибо нужно уточнять логику
            if (data.message && data.message[0]) {
              methods.insert_data.call(this, model, serialize(model, data.message[0]))
            } else {
              methods.insert_404.call(this, model, searchId, idToSearch)
            }
          })
        }

        return this.$store.getters[`entities/${model}/find`](searchId)
        // return this.$store.getters[`entities/${model}/query`]().where(idToSearch, searchId).first()
      },

      findAll (model, query = {}, page) {
        let result = []
        const ts = new Date().getTime()
        const fields = this.$store.getters[`entities/${model}/fields`](query.fields)
        let oldPage = this.$store.getters[`entities/${model}/page`](page)

        delete query.fields
        if (!oldPage || oldPage.ts + this.waitFor < ts || !page) {
          ax.get(`${model}/list`, {
            params: {
              fields,
              ...query
            }
          }).then(resp => {
            // Тут мог быть сериализатор
            const data = resp.data
            if (data.message && data.message.length > 0) {
              let paginationIds = []
              data.message.forEach(element => {
                methods.insert_data.call(this, model, serialize(model, element))
                result.push(this.$store.getters[`entities/${model}/find`](element.id))
                if (page) {
                  paginationIds.push(element.id)
                }
              })
              if (paginationIds.length > 0) {
                methods.insert_pagination_data.call(this, model, { page, ids: result })
              }
            }
          })
        }

        if (page) {
          return this.$store.getters[`entities/${model}/page`](page)
        } else {
          return this.$store.getters[`entities/${model}/all`]()
        }
      }
    }
  }
}

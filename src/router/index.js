/* eslint-disable */
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import EventList from '../views/EventList.vue'
import EventDetail from '../views/EventDetail.vue'

export default new Router({
  mode: 'history',
  base: '/ponominalu',
  routes: [
    {
      path: '/',
      name: 'EventsList',
      component: EventList
    },
    {
      path: '/:id',
      name: 'EventsDetail',
      component: EventDetail
    }
  ]
})

// Метод позволяющий инсертить дату во внутренью "Базу данных"
// https://vuex-orm.gitbooks.io/vuex-orm/store/inserting-and-updating-data.html
// Ну дальше комментировать не буду. По названиям должно быть понятно
export function insert_data (model, data) {
  this.$store.dispatch(`entities/${model}/insert`, {
    data
  })
}

export function insert_404 (model, id, searchingKey) {
  this.$store.commit(`entities/${model}/not_found`, { id, searchingKey })
}

export function insert_pagination_data (model, { page, ids }) {
  const pageData = this.$store.getters[`entities/${model}/page`](page)
  let shouldUpdate = pageData === false
  if (!shouldUpdate) {
    const incomingIdsOrder = Object.keys(ids)
    Object.keys(pageData).map((id, i) => {
      if (id !== incomingIdsOrder[i]) {
        shouldUpdate = true
      }
    })
  }
  if (shouldUpdate) {
    this.$store.commit(`entities/${model}/set_pagination`, { page, ids })
  }
}

const months = [
  'января',
  'февраля',
  'марта',
  'апреля',
  'мая',
  'июня',
  'июля',
  'августа',
  'сентября',
  'октября',
  'ноября',
  'декабря'
]

export default {
  methods: {
    getSimpleDate(dateCome) {
      const date = new Date(dateCome)
      const Day = date.getDay() > 9 ? date.getDay() : `0${date.getDay()}`
      const Month = months[date.getMonth()]
      const Hour = date.getHours() > 9 ? date.getHours() : `0${date.getHours()}`
      const Minute = date.getMinutes() > 9 ? date.getMinutes() : `0${date.getMinutes()}`
      return `${Day} ${Month}, ${Hour}:${Minute}`
    }
  }
}

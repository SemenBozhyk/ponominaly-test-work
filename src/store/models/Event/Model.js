import { Model } from '@vuex-orm/core'

export default class Event extends Model {
  static entity = 'events'

  // static primaryKey = ['code']

  // Так как это тестовое задание то такие поля как
  // subevents, categories не разбиваются на отдельные модели так как мне лень писать для этого сериализатор
  // был бы JSON API вместо REST API - была бы сказка =)
  static fields () {
    return {
      id: this.attr(null),
      title: this.attr(''),
      description: this.attr(''),
      subevents: this.attr([]),
      eticket_possible: this.attr(true),
      categories: this.attr([]),
      taggroups: this.attr([]),
      seo: this.attr({}),
      avg_weight: this.attr(0),
      code: this.attr(null),
      ts: this.attr(new Date().getTime())
    }
  }

  get serialize () {
    return Object.assign(this, { code: this.seo.alias })
  }
}
